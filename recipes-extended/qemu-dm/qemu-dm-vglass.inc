FILESEXTRAPATHS_prepend := "${THISDIR}/patches:"

DEPENDS += " \
    libpvglass \
    virtual/libivc \
"

SRC_URI += " \
    file://vglass-display-change-listener.patch \
"

EXTRA_OECONF += "--enable-vglass"

RDEPENDS_${PN} += " \
    libxkbcommon \
    libpvdisplayhelper \
"

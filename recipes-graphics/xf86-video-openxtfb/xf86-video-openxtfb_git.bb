require recipes-graphics/xorg-driver/xorg-driver-video.inc
LIC_FILES_CHKSUM = "file://COPYING;md5=d8cbd99fff773f92e844948f74ef0df8"

DESCRIPTION = "X.Org X server -- OpenXT framebuffer display driver"

DEPENDS = " \
    xserver-xorg \
    kernel-module-openxtfb \
"

PV = "0+git${SRCPV}"

SRC_URI = " \
    git://gitlab.com/vglass/xf86-video-openxtfb.git;protocol=https;branch=master \
    file://10-openxtfb.rules \
    file://display_hotplug.sh \
"
SRCREV = "${AUTOREV}"

S = "${WORKDIR}/git"

do_install_append() {
    install -d "${D}${sysconfdir}/udev/rules.d"
    install -m 644 "${WORKDIR}/10-openxtfb.rules" "${D}${sysconfdir}/udev/rules.d/10-openxtfb.rules"
    install -d "${D}${datadir}/openxt"
    install -m 755 "${WORKDIR}/display_hotplug.sh" "${D}${datadir}/openxt/display_hotplug.sh"
}

FILES_${PN} += "  \
    ${sysconfdir}/udev/rules.d/10-openxtfb.rules \
    ${datadir}/openxt/display_hotplug.sh \
"

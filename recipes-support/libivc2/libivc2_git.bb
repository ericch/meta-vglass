SUMMARY = "libivc specific for dom0"
DESCRIPTION = "This recipe builds/provides libivc for dom0."
LIC_FILES_CHKSUM = "file://../../../LICENSE;md5=1676875b46a1978a82e9a6816db4e8a5"
LICENSE = "BSD"

DEPENDS = " \
    qtbase \
    libxenbe \
"

PROVIDES = "virtual/libivc"

PV = "2+git${SRCPV}"
SRC_URI = "git://gitlab.com/vglass/ivc.git;protocol=https;branch=master"
SRCREV = "${AUTOREV}"

S = "${WORKDIR}/git/src/usivc/ivclib"

require recipes-qt/qt5/qt5.inc

RCONLFICTS_${PN} = "libivc"

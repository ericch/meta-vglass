SUMMARY = "Framebuffer Driver for OpenXT Linux Guests"
DESCRIPTION = " \
    Simple framebuffer driver for OpenXT Linux Guests using the new Display \
    Handler. Supports single or multiple framebuffers, which can provide \
    simple single-monitor or multi-monitor support (when used with a \
    software renderer, like Xinerama). \
"
AUTHOR = "Kyle J. Temkin <temkink@ainfosec.com>"
HOMEPAGE = "http://www.ainfosec.com"
SECTION  = "kernel/modules"

LICENSE = "GPL"
LIC_FILES_CHKSUM = "file://LICENSE;md5=b93b8e559b6be52894479d52aa1ccb6c"

DEPENDS = " \
    kernel-module-ivc \
    kernel-module-pv-display-helper \
"
PROVIDES = "openxtfb"

PV = "0+git${SRCPV}"

SRC_URI = "git://gitlab.com/vglass/openxtfb.git;protocol=https;branch=master"
SRCREV = "${AUTOREV}"

S = "${WORKDIR}/git"

inherit module

EXTRA_OEMAKE += " \
    IVC_INCLUDE_DIR=${RECIPE_SYSROOT}/usr/src/ivc/include \
    PDH_INCLUDE_DIR=${RECIPE_SYSROOT}/usr/src/pv_display_helper/include \
    INSTALL_HDR_PATH=${D}${prefix} \
"
MODULES_INSTALL_TARGET += "headers_install"

RPROVIDES_${PN} = "openxtfb"
